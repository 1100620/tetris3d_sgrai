#include "Board.h"

Board::Board()
{
}

void Board::draw_ground() {

	glBegin(GL_LINES);
	glColor3f(0.5f, 0.5f, 0.5f);
	for (int i = 0; i <= GROUND_GRID_SIZE; i++) {
		glVertex3f(i, 0, 0);
		glVertex3f(i, GROUND_GRID_SIZE, 0);
		glVertex3f(0, i, 0);
		glVertex3f(GROUND_GRID_SIZE, i, 0);
	}
	glEnd();
}

void Board::draw_walls() {
	glBegin(GL_LINES);
	glColor3f(0.8f, 0.8f, 0.8f);
	for (int i = 0; i <= WALL_GRID_SIZE; i++) {
		if (i > GROUND_GRID_SIZE) {

			glVertex3f(0, 0, i);
			glVertex3f(GROUND_GRID_SIZE, 0, i);
		}
		else {
			glVertex3f(i, 0, 0);
			glVertex3f(i, 0, WALL_GRID_SIZE);
			glVertex3f(0, 0, i);
			glVertex3f(GROUND_GRID_SIZE, 0, i);

		}

	}
	glEnd();

	glBegin(GL_LINES);
	glColor3f(0.8f, 0.8f, 0.8f);
	for (int i = 0; i <= WALL_GRID_SIZE; i++) {
		if (i > GROUND_GRID_SIZE) {
			glVertex3f(0, 0, i);
			glVertex3f(0, GROUND_GRID_SIZE, i);
		}
		else {
			glVertex3f(0, i, 0);
			glVertex3f(0, i, WALL_GRID_SIZE);
			glVertex3f(0, 0, i);
			glVertex3f(0, GROUND_GRID_SIZE, i);
		}

	}
	glEnd();
}


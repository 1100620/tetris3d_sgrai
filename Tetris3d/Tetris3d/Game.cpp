#include "Game.h"
#include "Block.h"

int Game::w = 1040, Game::h = 768, Game::block_number = Game::randNumber();
float Game::fps = 60.f;
bool Game::ground_flag;
Board Game::board;

Block * block;
float matrix[10][8][8];

int num;
void RenderScene1();

std::queue<Block *> base;

Game::Game()
{
	glutInitWindowPosition(0, 0);
	glutInitWindowSize(w, h);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	if (glutCreateWindow("Tetris 3D") == GL_FALSE)
		exit(1);

	Init();
	setLight();
	setMaterial();
	glutDisplayFunc(Draw);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Teclado);
	glutSpecialFunc(SpecialInput);
	glutMouseFunc(Mouse);
	glutIdleFunc(Update); //timer



}


/* Inicialização do ambiente OPENGL */
void Game::Init()
{
	srand((unsigned)time(NULL));

	modelo.parado = GL_TRUE;

	estado.debug = DEBUG;
	estado.menuActivo = GL_FALSE;
	estado.delayMovimento = DELAY_MOVIMENTO;
	estado.camera.eye.x = 10;
	estado.camera.eye.y = 10;
	estado.camera.eye.z = 15;
	estado.camera.center.x = 0;
	estado.camera.center.y = 0;
	estado.camera.center.z = 0;
	estado.camera.up.x = 0;
	estado.camera.up.y = 0;
	estado.camera.up.z = 1;
	estado.ortho = GL_TRUE;
	estado.camera.fov = 40;

	num = 5;

	block = new Block(1, 1, WALL_GRID_SIZE, num);

	initializeMatrix();



	estado.teclas.a = estado.teclas.q = estado.teclas.z = estado.teclas.x = \
		estado.teclas.up = estado.teclas.down = estado.teclas.left = estado.teclas.right = GL_FALSE;

	glClearColor(0.0, 0.0, 0.0, 0.0);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(0, w, 0, h, -1, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_POINT_SMOOTH);
	glEnable(GL_LINE_SMOOTH);
	glEnable(GL_POLYGON_SMOOTH);
	glEnable(GL_DEPTH_TEST);
	//glutIgnoreKeyRepeat(GL_TRUE);
}

void Game::callBlock() {
	//cout << " X: " << block->getPosx() << " || Y: " << block->getPosy() << " || Z: " << block->getPosz() << endl;
	//cout << " X: " << block->blockComposition[0].getPosx() << " Y: " << block->blockComposition[0].getPosy() << " Z: " << block->blockComposition[0].getPosz();
	if (block->getPosz() == 10) {
		if (!Game::ground_flag) {
			Game::ground_flag = true;
			Game::block_number = Game::randNumber();
			block->drawBlock(block_number);
		}
		else {
			block->drawBlock(block_number);
		}
	}
	else {
		Game::ground_flag = false;
		block->drawBlock(block_number);
	}
}

void Game::Draw()
{


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	switch (g_eCurrentScene)
	{
	case ST_Scene1:
	{
		RenderScene1();
	}
	break;
	case ST_Scene2:
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		gluPerspective(estado.camera.fov, (GLfloat)w / h, 1, 100);
		gluLookAt(estado.camera.eye.x, estado.camera.eye.y, estado.camera.eye.z, \
			estado.camera.center.x, estado.camera.center.y, estado.camera.center.z, \
			0, 0, 1);

		DrawBoard();

		glPushMatrix();
		//old// glTranslatef(block->getPosx() - 0.5, block->getPosy() - 0.5, block->getPosz());
		glTranslatef(block->getPosx(), block->getPosy(), block->getPosz());
		callBlock();
		glPopMatrix();

		glPushMatrix();
		//glTranslatef(- 0.5, - 0.5, 0);
		for (int z = 0; z < WALL_GRID_SIZE; z++) {
			for (int x = 0; x < GROUND_GRID_SIZE; x++) {
				for (int y = 0; y < GROUND_GRID_SIZE; y++) {
					//cout << matrix[z][x][y] << " ";
					if (matrix[z][x][y] == 1) {
						glPushMatrix();
						glTranslatef(x, y, z);
						GLfloat * color = block->color;
						Cube::drawCube(color);
						glPopMatrix();
					}
				}
			}
		}
		glPopMatrix();

		bool checkLine;


		checkLine = false;
		glPushMatrix();
		//glTranslatef(-0.5, -0.5, 0);
		for (int z = 0; z < WALL_GRID_SIZE; z++) {
			for (int x = 0; x < GROUND_GRID_SIZE; x++) {
				for (int y = 0; y < GROUND_GRID_SIZE; y++) {
					if (matrix[z][x][y] == 1) {
						checkLine = true;
					}
					else {
						checkLine = false;
					}
				}
				if (checkLine == true) {

					for (int yy = 0; yy < GROUND_GRID_SIZE; yy++) {
						matrix[z][x][yy] = 0;
					}
					// fazer translate -1 ao zz para apagar a primeira base toda?
				}
			}
			checkLine = false;
		}
		glPopMatrix();

		checkLine = false;
		glPushMatrix();
		//glTranslatef(-0.5, -0.5, 0);
		for (int z = 0; z < WALL_GRID_SIZE; z++) {
			for (int y = 0; y < GROUND_GRID_SIZE; y++) {
				for (int x = 0; x < GROUND_GRID_SIZE; x++) {
					if (matrix[z][x][y] == 1) {
						checkLine = true;
					}
					else {
						checkLine = false;
					}
				}
				if (checkLine == true) {

					for (int xx = 0; xx < GROUND_GRID_SIZE; xx++) {
						matrix[z][xx][y] = 0;
					}
					// fazer translate -1 ao zz para apagar a primeira base toda?
				}
			}
			checkLine = false;
		}
		glPopMatrix();

	}
	break;
	}
	glutSwapBuffers();


}

void DrawTriangle(float2 p1, float2 p2, float2 p3)
{
	glBegin(GL_TRIANGLES);
	glVertex2f(p1.x, p1.y);
	glVertex2f(p2.x, p2.y);
	glVertex2f(p3.x, p3.y);
	glEnd();
}

void DrawRectangle(float width, float height)
{
	const float w = width / 2.0f;
	const float h = height / 2.0f;

	glBegin(GL_QUADS);
	glVertex2f(-w, h);   // Top-Left
	glVertex2f(w, h);   // Top-Right
	glVertex2f(w, -h);   // Bottom-Right
	glVertex2f(-w, -h);   // Bottom-Left
	glEnd();

}

void DrawCircle(float radius, int numSides /* = 8 */)
{
	const float step = M_PI / numSides;
	glBegin(GL_TRIANGLE_FAN);
	glVertex2f(0.0f, 0.0f);
	for (float angle = 0.0f; angle < (2.0f * M_PI); angle += step)
	{
		glVertex2f(radius * sinf(angle), radius * cosf(angle));
	}
	glVertex2f(0.0f, radius); // One more vertex to close the circle
	glEnd();
}

void RenderScene1()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(0.0f, 0.0f, 0.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	DrawRectangle(3.0f, 2.0f);

}

void Game::Reshape(int width, int height)
{

	glViewport(0, 0, (GLint)width, (GLint)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();


	if (width < height)
		glOrtho(-2, 2, -2 * (GLdouble)height / width, 2 * (GLdouble)height / width, -100, 100);
	else
		glOrtho(-2 * (GLdouble)width / height, 2 * (GLdouble)width / height, -2, 2, -100, 100);


	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void Game::seleciona_botao(int x, int y) {
	g_eCurrentScene = ST_Scene2;
}

void Game::Mouse(int button, int state, int x, int y) {
	switch (button) {
	case GLUT_LEFT_BUTTON:
		if (state == GLUT_DOWN)
		{
			seleciona_botao(x, y);
		}
		break;
	case GLUT_RIGHT_BUTTON:
		if (state == GLUT_DOWN)
		{
			g_eCurrentScene = ST_Scene1;
		}
		break;
	}

	if (DEBUG)
		printf("Mouse button:%d state:%d coord:%d %d\n", button, state, x, y);
}


void Game::SpecialInput(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_UP:
	{
		Block::block_ang_rot_y += 90;
		if (Block::block_ang_rot_y >= 360) {
			Block::block_ang_rot_y = 0;
		}
	}
	break;
	case GLUT_KEY_DOWN:
	{
		Block::block_ang_rot_y -= 90;
		if (Block::block_ang_rot_y <= -360) {
			Block::block_ang_rot_y = 0;
		}
	}
	break;
	case GLUT_KEY_LEFT:
	{
		Block::block_ang_rot_z -= 90;
		if (Block::block_ang_rot_z <= -360) {
			Block::block_ang_rot_z = 0;
		}
	}


	break;
	case GLUT_KEY_RIGHT:

	{
		Block::block_ang_rot_z += 90;
		if (Block::block_ang_rot_z >= 360) {
			Block::block_ang_rot_z = 0;
		}
	}

	break;
	}
}


void Game::Teclado(unsigned char c, int x, int y)
{
	switch (c) {
	case 's':

		if (block->getPosx() < 7) {
			block->setPosx(block->getPosx() + 1);
		}

		break;
	case 'w':
		if (block->getPosx() > 0) {
			block->setPosx(block->getPosx() - 1);
		}
		break;
	case 'd':
		if (block->getBlockNumber() == 1) {
			if (block->getPosy() < 5) {
				block->setPosy(block->getPosy() + 1);
			}
		}
		else if (block->getBlockNumber() == 2) {
			if (block->getPosy() < 6) {
				block->setPosy(block->getPosy() + 1);
			}
		}
		else {
			if (block->getPosy() < 7) {
				block->setPosy(block->getPosy() + 1);
			}
		}
		break;
	case 'a':
		if (block->getPosy() > 0) {
			block->setPosy(block->getPosy() - 1);
		}
		break;
	case 'm':
		if (block->getPosz() > 0) {
			block->setPosz(block->getPosz() - 1);
		}
		break;
	case 'q':
		Block::block_ang_rot_x += 90;

		break;
	case 'e':

		Block::block_ang_rot_x -= 90;

		break;
	case '1':

		g_eCurrentScene = ST_Scene1;

		break;

	case '2':

		g_eCurrentScene = ST_Scene2;
	}
}

void Game::Update()
{

	if (g_eCurrentScene == ST_Scene2) {

		static float tempo = 0;
		static float updateBlock = 0;

		// 60 frames em 60 frames
		if (glutGet(GLUT_ELAPSED_TIME) - (tempo + 1.f / fps) > 0) {

			if (glutGet(GLUT_ELAPSED_TIME) > updateBlock + 200.f) {
				updateBlock = glutGet(GLUT_ELAPSED_TIME);
				block->setPosz(block->getPosz() - 1);
				if (block->getPosz() <= 0) {
					cout << (int)block->getPosz() << " ";
					cout << (int)block->getPosx() << " ";
					cout << (int)block->getPosy() << endl;
					matrix[(int)block->getPosz()][(int)block->getPosx()][(int)block->getPosy()] = 1;
					block->setPosz(WALL_GRID_SIZE);
				}

			}


			tempo = glutGet(GLUT_ELAPSED_TIME);
			glutPostRedisplay();
		}
	}
	else {
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		glutPostRedisplay();
	}
}


void Game::DrawBoard()
{
	board.draw_ground();
	board.draw_walls();
}

int Game::randNumber()
{
	srand(time(NULL));
	int iRand = (rand() % 5) + 1;
	return iRand;
}



/*
case 'L':
estado.localViewer=!estado.localViewer;
*/
void Game::setLight()
{
	GLfloat light_pos[4] = { 8.0, 8.0, 10.0, 0.0 };
	GLfloat light_ambient[] = { 0.2f, 0.2f, 0.2f, 1.0f };
	GLfloat light_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	GLfloat light_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };

	// ligar iluminação
	glEnable(GL_LIGHTING);

	// ligar e definir fonte de luz 0
	glEnable(GL_LIGHT0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
	glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, estado.localViewer);

	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, estado.localViewer);
}

void Game::setMaterial()
{
	GLfloat mat_specular[] = { 0.0f, 0.0f, 0.0f, 1.0f };
	GLfloat mat_shininess = 0;

	// criação automática das componentes Ambiente e Difusa do material a partir das cores
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

	// definir de outros parâmetros dos materiais estáticamente
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialf(GL_FRONT, GL_SHININESS, mat_shininess);
}

void Game::initializeMatrix()
{
	for (int z = 0; z < WALL_GRID_SIZE; z++) {
		for (int x = 0; x < GROUND_GRID_SIZE; x++) {
			for (int y = 0; y < GROUND_GRID_SIZE; y++) {
				matrix[z][x][y] = 0;
			}
		}
	}
}


int main(int argc, char **argv) {


	glutInit(&argc, argv);

	Game game;


	glutMainLoop();
	return 0;
}

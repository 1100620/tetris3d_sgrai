#include "Cube.h"


void Cube::drawCube(GLfloat * color)
{
	
	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
	{ 0.5,-0.5,-0.5 },
	{ 0.5,0.5,-0.5 },
	{ -0.5,0.5,-0.5 },
	{ -0.5,-0.5,0.5 },
	{ 0.5,-0.5,0.5 },
	{ 0.5,0.5,0.5 },
	{ -0.5,0.5,0.5 } };
	GLfloat normais[][3] = { { 0,0,-1 },{ 0,1,0 },{ -1,0,0 },{ 1,0,0 },{ 0,0,1 },{ 0,1,0 } };


	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], normais[0], color);
	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], normais[1], color);
	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], normais[2], color);
	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], normais[3], color);
	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], normais[4], color);
	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], normais[5], color);
}


void Cube::desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat d[], GLfloat normal[], GLfloat color[])
{
	
	glBegin(GL_POLYGON);
	glColor3fv(color);
	glNormal3fv(normal);
	glVertex3fv(a);
	glVertex3fv(b);
	glVertex3fv(c);
	glVertex3fv(d);
	glEnd();
	
	/*
	glColor3f(color[0], color[1],color[2]);
	glutSolidCube(1);
	glColor3f(0, 0, 0);
	glutWireCube(1.0);
	*/
}



////Sem as normais
//void Cube::drawCube(GLfloat * color)
//{
//
//	GLfloat vertices[][3] = { { -0.5,-0.5,-0.5 },
//	{ 0.5,-0.5,-0.5 },
//	{ 0.5,0.5,-0.5 },
//	{ -0.5,0.5,-0.5 },
//	{ -0.5,-0.5,0.5 },
//	{ 0.5,-0.5,0.5 },
//	{ 0.5,0.5,0.5 },
//	{ -0.5,0.5,0.5 } };
//	//GLfloat normais[][3] = { { 0,0,-1 },{ 0,1,0 },{ -1,0,0 },{ 1,0,0 },{ 0,0,1 },{ 0,1,0 } };
//
//
//	desenhaPoligono(vertices[1], vertices[0], vertices[3], vertices[2], color);
//	desenhaPoligono(vertices[2], vertices[3], vertices[7], vertices[6], color);
//	desenhaPoligono(vertices[3], vertices[0], vertices[4], vertices[7], color);
//	desenhaPoligono(vertices[6], vertices[5], vertices[1], vertices[2], color);
//	desenhaPoligono(vertices[4], vertices[5], vertices[6], vertices[7], color);
//	desenhaPoligono(vertices[5], vertices[4], vertices[0], vertices[1], color);
//}

////Sem as normais
//void Cube::desenhaPoligono(GLfloat a[], GLfloat b[], GLfloat c[], GLfloat d[], GLfloat color[])
//{
//
//	glBegin(GL_POLYGON);
//	glColor3fv(color);
//	glVertex3fv(a);
//	glVertex3fv(b);
//	glVertex3fv(c);
//	glVertex3fv(d);
//	glEnd();
//
//	/*
//	glColor3f(color[0], color[1],color[2]);
//	glutSolidCube(1);
//	glColor3f(0, 0, 0);
//	glutWireCube(1.0);
//	*/
//}

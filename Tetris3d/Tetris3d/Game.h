#ifndef GAME_H
#define GAME_H

#pragma once
#define _USE_MATH_DEFINES
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <queue>
#include "Board.h"
#include "Block.h"
#include <GL/glut.h>

using namespace std;

#define DEBUG               0
#define DELAY_MOVIMENTO     5

typedef struct {
	GLboolean   parado;
}Modelo;

typedef struct {
	GLfloat    x, y, z;
}Pos;

typedef struct {
	Pos      eye, center, up;
	GLfloat  fov;
}Camera;

typedef struct {
	GLboolean   q, a, z, x, up, down, left, right;
}Teclas;


typedef struct {
	GLboolean   doubleBuffer;
	GLint       delayMovimento;
	Teclas      teclas;
	GLuint      menu_id;
	GLboolean   menuActivo;
	Camera      camera;
	GLboolean   debug;
	GLboolean   ortho;
	GLboolean   localViewer;
	GLboolean   menuInicio = GL_TRUE;
	GLboolean   menuJogo = GL_FALSE;
	GLboolean   start;
	GLboolean   exit;

}Estado;

enum ESceneType
{
	ST_Scene1 = 0,
	ST_Scene2
};
ESceneType g_eCurrentScene = ST_Scene1;

struct float2
{
	float2(float _x = 0.0f, float _y = 0.0f) : x(_x), y(_y) {}

	float x;
	float y;
};

struct float3
{
	float3(float _x = 0.0f, float _y = 0.0f, float _z = 0.0f) : x(_x), y(_y), z(_z) {}

	float x;
	float y;
	float z;
};

typedef struct {
	GLfloat       x, y;
}EstBotao;

typedef struct {
	EstBotao start, exit;
}Botoes;

Estado estado;
Modelo modelo;
Botoes botoes;

#define COMPRIMENTO_BOTAO 40
#define LARGURA_BOTAO 150

#define LARGURA_ECRA 640
#define COMPRIMENTO_ECRA 480
#define ESPACO_ENTRE_BOTOES 100


class Game {
public:
	Game();
	static void Init();
	static void Draw();
	static void callBlock();
	static void Reshape(int width, int height);
	static void Teclado(unsigned char c, int x, int y);
	static void SpecialInput(int key, int x, int y);
	static void Update();
	static void DrawBoard();
	static void DrawMenu();
	static int randNumber();
	static void setLight();
	static void setMaterial();


private:
	static int w, h, block_number;
	static bool ground_flag;
	static float fps; //frames per second
	static Board board;
	static void initializeMatrix();
	static void Mouse(int button, int state, int x, int y);
	static void seleciona_botao(int x, int y);
};


#endif
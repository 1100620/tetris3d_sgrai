#ifndef BLOCK_H
#define BLOCK_H

#pragma once

#include <stdio.h>      /* printf, scanf, puts, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <iostream>
#include <queue>
#include "Cube.h"
#include <GL/glut.h>
/*
GLfloat red[3] = { 1,0,0 };
GLfloat green[3] = { 0,1,0 };
GLfloat blue[3] = { 0,0,1 };
GLfloat yellow[3] = { 1,1,0 };
GLfloat purple[3] = { 1,0,1 };
*/

using namespace std;


class Block
{
public:
	Block();
	Block(float x, float y, float z,int n);
	static void drawBlock(int rnumber);
	static void update();
	float getPosx() { return pos_x; }
	float getPosy() { return pos_y; }
	float getPosz() { return pos_z; }
	void setPosx(float posx) { pos_x = posx; }
	void setPosy(float posy) { pos_y = posy; }
	void setPosz(float posz) { pos_z = posz; }
	int getBlockNumber() { return blockNumber; }
	void setBlockNumber(int n) { blockNumber = n; }
	static int block_ang_rot_x, block_ang_rot_y, block_ang_rot_z;
	static Cube blockComposition[4];
	static GLfloat * color;
	

private:

	float pos_x;
	float pos_y;
	float pos_z;
	int blockNumber;
	
};

#endif

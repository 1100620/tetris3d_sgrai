#include "Block.h"

GLfloat red[3] = { 1,0,0 };
GLfloat green[3] = { 0,1,0 };
GLfloat blue[3] = { 0,0,1 };
GLfloat yellow[3] = { 1,1,0 };
GLfloat purple[3] = { 1,0,1 };
int Block::block_ang_rot_x, Block::block_ang_rot_y, Block::block_ang_rot_z;
Cube Block::blockComposition[4];
GLfloat * Block::color;


Block::Block()
{
}

Block::Block(float x, float y, float z, int n)
{
	setPosx(x);
	setPosy(y);
	setPosz(z);
	setBlockNumber(n);
}

void Block::drawBlock(int rnumber) {

	glRotatef(Block::block_ang_rot_x, 1.0, 0, 0);
	//cout << "Angulo X: " << Block::block_ang_rot_x;

	glRotatef(Block::block_ang_rot_y, 0, 1.0, 0);
	//cout << "Angulo Y: " << Block::block_ang_rot_y;

	glRotatef(Block::block_ang_rot_z, 0, 0, 1.0);
	//cout << "Angulo Z: " << Block::block_ang_rot_z;
	


	if (rnumber == 1)
	{
		color=red;
		//barra de 4 cubos //Red 1
		glPushMatrix();

		glPushMatrix();
		//Cube cube1(0, 0, 0);
		//blockComposition[0]=cube1;
		//cube1.drawCube(red);
		Cube::drawCube(red);
		glPopMatrix();

		glTranslatef(0, 1, 0);

		glPushMatrix();
		//Cube cube2(0, 1, 0);
		//blockComposition[1]=cube2;
		//cube2.drawCube(red);
		Cube::drawCube(red);
		glPopMatrix();

		glTranslatef(0, 1, 0);

		glPushMatrix();
		//Cube cube3(0, 2, 0);
		//blockComposition[2]=cube3;
		//cube3.drawCube(red);
		Cube::drawCube(red);
		glPopMatrix();

		glTranslatef(0, 1, 0);

		glPushMatrix();
		//Cube cube4(0, 3, 0);
		//blockComposition[3]=cube4;
		Cube::drawCube(red);
		glPopMatrix();

		glPopMatrix();
	}
	else if (rnumber == 2) {
		//t com 4 cubos  // Green - 2
		color = green;
		glPushMatrix();

		glPushMatrix();
		Cube::drawCube(green);
		glPopMatrix();

		glTranslatef(0, 1, 0);

		glPushMatrix();
		Cube::drawCube(green);
		glPopMatrix();

		glTranslatef(0, 1, 0);

		glPushMatrix();
		Cube::drawCube(green);
		glPopMatrix();

		glTranslatef(0, -1, -1);

		glPushMatrix();
		Cube::drawCube(green);
		glPopMatrix();

		glPopMatrix();
	}
	else if (rnumber == 3) {
		color = blue;
		//quadrado com 4 cubos   // blue - 3
		glPushMatrix();

		glPushMatrix();
		Cube::drawCube(blue);
		glPopMatrix();

		glTranslatef(0, 1, 0);

		glPushMatrix();
		Cube::drawCube(blue);
		glPopMatrix();

		glTranslatef(0, -1, 1);

		glPushMatrix();
		Cube::drawCube(blue);
		glPopMatrix();

		glTranslatef(0, 1, 0);

		glPushMatrix();
		Cube::drawCube(blue);
		glPopMatrix();

		glPopMatrix();
	}
	else if (rnumber == 4) {
		color = yellow;
		//L com 4 cubos   // yellow - 4
		glPushMatrix();

		glPushMatrix();
		Cube::drawCube(yellow);
		glPopMatrix();

		glTranslatef(0, 0, -1);

		glPushMatrix();
		Cube::drawCube(yellow);
		glPopMatrix();

		glTranslatef(0, 0, -1);

		glPushMatrix();
		Cube::drawCube(yellow);
		glPopMatrix();


		glTranslatef(0, 1, 0);

		glPushMatrix();
		Cube::drawCube(yellow);
		glPopMatrix();
	}
	else if (rnumber == 5) {
		color = purple;
		//S com 4 cubos   // purple - 5
		glPushMatrix();

		glPushMatrix();
		Cube::drawCube(purple);
		glPopMatrix();

		glTranslatef(0, 1, 0);

		glPushMatrix();
		Cube::drawCube(purple);
		glPopMatrix();

		glTranslatef(0, 0, -1);

		glPushMatrix();
		Cube::drawCube(purple);
		glPopMatrix();


		glTranslatef(0, -1, 2);

		glPushMatrix();
		Cube::drawCube(purple);
		glPopMatrix();
	}


	glPopMatrix();

}

void Block::update()
{

}



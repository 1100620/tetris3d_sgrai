#ifndef BOARD_H
#define BOARD_H

#pragma once

#include <iostream>
#include <GL/glut.h>

using namespace std;

#define GROUND_GRID_SIZE 8
#define WALL_GRID_SIZE 10

class Board {

public:
	Board();
	static void draw_ground();
	static void draw_walls();


private:


};


#endif
